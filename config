set $mod Mod4

set $font pango:Ubuntu Mono 12

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font $font

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+Escape kill

# Launch bindings
bindsym $mod+Return exec rofi -show run -show-icons &
bindsym $mod+Control+Return exec rofi -show window &

bindsym $mod+i exec term &
bindsym $mod+Mod1+i exec term -e ptpython &
bindsym $mod+g exec browser-nw &
bindsym $mod+Shift+g exec firefox --private-window &
bindsym $mod+e exec term -e nvim &
bindsym $mod+c exec gsimplecal &
bindsym $mod+F6 exec set-inputs  # set up keyboard map & trackpad

# Display updates
bindsym $mod+F7 exec autorandr.py --change --force, exec set-inputs
bindsym $mod+Control+F7 exec reset-displays, exec autorandr.py --load common --force  # for presentations

bindsym XF86AudioLowerVolume exec volume -n -d 5
bindsym XF86AudioRaiseVolume exec volume -n -i 5
bindsym XF86AudioMute exec volume -n -m

bindsym $mod+F1 exec playerctl previous
bindsym $mod+F2 exec playerctl play-pause
bindsym $mod+F3 exec playerctl stop
bindsym $mod+F4 exec playerctl next

bindsym XF86AudioPrev exec playerctl previous
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioStop exec playerctl stop
bindsym XF86AudioNext exec playerctl next

bindsym XF86MonBrightnessDown exec xbacklight -perceived -dec 3
bindsym XF86MonBrightnessUp exec xbacklight -perceived -inc 3

bindsym Scroll_Lock exec xset s activate 
bindsym XF86Calculator exec xset s activate 

bindsym Print exec gnome-screenshot --interactive

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

bindsym $mod+period [urgent=latest] focus

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# flip through workspaces
bindsym $mod+Left workspace prev_on_output
bindsym $mod+Right workspace next_on_output
bindsym $mod+Control+h workspace prev_on_output
bindsym $mod+Control+l workspace next_on_output
bindsym $mod+Shift+Left move container to workspace prev, workspace prev_on_output
bindsym $mod+Shift+Right move container to workspace next, workspace next_on_output

# change output focus
bindsym $mod+bracketleft focus output left
bindsym $mod+bracketright focus output right

# Move windows between outputs
bindsym $mod+Shift+bracketleft move container to output left, focus output left
bindsym $mod+Shift+bracketright move container to output right, focus output right

# Move workspaces between outputs
bindsym $mod+Control+Shift+bracketleft move workspace to output left
bindsym $mod+Control+Shift+bracketright move workspace to output right

# change container splits
bindsym $mod+equal split h
bindsym $mod+minus split v

# change container layout
bindsym $mod+Tab layout toggle split
bindsym $mod+grave layout stacking
bindsym $mod+Shift+grave layout tabbed

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle
bindsym $mod+s sticky toggle
bindsym $mod+b border toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus container relationships
bindsym $mod+a focus parent
bindsym $mod+Shift+a focus child
bindsym $mod+o focus prev 
bindsym $mod+p focus next

# "tuck" current window under previous to create a vertical pile
bindsym $mod+t mark _src,\
    focus prev,\
    split v,\
    mark _dst,\
    [con_mark="_src"] focus,\
    move window to mark _dst,\
    unmark _src,\
    unmark _dst,

bindsym $mod+Shift+BackSpace move scratchpad
bindsym $mod+BackSpace scratchpad show

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10
bindsym $mod+F8 workspace "fastmail"
bindsym $mod+F9 workspace "todo"
bindsym $mod+F10 workspace "gmail"
bindsym $mod+F11 workspace "chat"
bindsym $mod+F12 workspace "music"

bindsym $mod+n exec --no-startup-id ~/.config/i3/workspace new
bindsym $mod+Shift+n exec ~/.config/i3/workspace move-to-new
bindsym $mod+w exec ~/.config/i3/workspace switch
bindsym $mod+Shift+w exec ~/.config/i3/workspace move
bindsym $mod+Control+n exec i3-input -f "$font" -F 'rename workspace to "%s"' -P 'New name: '

bindsym $mod+z workspace back_and_forth

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1, workspace 1
bindsym $mod+Shift+2 move container to workspace 2, workspace 2
bindsym $mod+Shift+3 move container to workspace 3, workspace 3
bindsym $mod+Shift+4 move container to workspace 4, workspace 4
bindsym $mod+Shift+5 move container to workspace 5, workspace 5
bindsym $mod+Shift+6 move container to workspace 6, workspace 6
bindsym $mod+Shift+7 move container to workspace 7, workspace 7
bindsym $mod+Shift+8 move container to workspace 8, workspace 8
bindsym $mod+Shift+9 move container to workspace 9, workspace 9
bindsym $mod+Shift+0 move container to workspace 10, workspace 10
bindsym $mod+Shift+F8 move container to workspace "fastmail", workspace "fastmail"
bindsym $mod+Shift+F9 move container to workspace "todo", workspace "todo"
bindsym $mod+Shift+F10 move container to workspace "gmail", workspace "gmail"
bindsym $mod+Shift+F11 move container to workspace "chat", workspace "chat"
bindsym $mod+Shift+F12 move container to workspace "music", workspace "music"

set $defaultmode mode "default"

# resize window (you can also use the mouse for that)
mode "resize" {
    bindsym h resize shrink width 100 px or 5 ppt
    bindsym Left resize shrink width 100 px or 5 ppt

    bindsym j resize grow height 100 px or 5 ppt
    bindsym Down resize grow height 100 px or 5 ppt

    bindsym k resize shrink height 100 px or 5 ppt
    bindsym Up resize shrink height 100 px or 5 ppt

    bindsym l resize grow width 100 px or 5 ppt
    bindsym Right resize grow width 100 px or 5 ppt

    # back to normal: Enter or Escape
    bindsym Return $defaultmode
    bindsym Escape $defaultmode
}
bindsym $mod+r mode "resize"

# mode for system level functions
mode "system"{
     bindsym q exit
     bindsym s $defaultmode, exec systemctl suspend
     bindsym r exec systemctl reboot
     bindsym p exec systemctl poweroff
     bindsym l exec xset s activate

     bindsym Return $defaultmode
     bindsym Escape $defaultmode
}
bindsym Pause mode "system"
bindsym $mod+XF86HomePage mode "system"
bindsym $mod+Control+s mode "system"


# reload the configuration file
bindsym $mod+Shift+r reload

# restart i3 inplace
# (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Control+r restart

# Exit / logout
bindsym $mod+Control+x exit

# rules
for_window [window_role="pop-up"] floating enable
for_window [window_role="task_dialog"] floating enable
for_window [class="Gsimplecal"] floating enable, border pixel 2

default_border pixel 2
default_floating_border pixel 2

# class                  border  backgr. text    indcatr child_border
client.focused           #f6d55c #20639b #cfcfcf #999900 #f6d55c
client.focused_inactive  #3caea3 #173f5f #cfcfcf #5f676a #222222
client.unfocused         #173f5f #173f5f #cfcfcf #292d2e #222222
# client.urgent           #2f343a #900000 #ffffff #900000 #AA6DB6
# client.placeholder      #000000 #0c0c0c #ffffff #000000 #0c0c0c

client.background       #ffffff

