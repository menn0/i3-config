#!/usr/bin/python3

import json
import subprocess as proc
import sys

MIN_WORKSPACE = 1  

def list_workspaces():
    for name in workspace_names():
        print(name)


def create_new_workspace():
   proc.check_call(["i3-msg", "workspace", str(find_free_workspace())])


def move_container_to_new_workspace():
    workspace = str(find_free_workspace())
    move_container_to_workpace(workspace)
    switch_workspace(workspace)


def select_and_switch_workspace():
    workspace = select_workspace()
    if workspace:
        switch_workspace(workspace)


def move_container_to_selected_workspace():
    workspace = select_workspace()
    if workspace:
        move_container_to_workpace(workspace)
        switch_workspace(workspace)


def main():
    args = sys.argv[1:]
    command = ""
    if len(args) > 0:
        command = args[0]

    if command == "":
        list_workspaces()
    elif command == "new":
        create_new_workspace()
    elif command == "move-to-new":
        move_container_to_new_workspace()
    elif command == "switch":
        select_and_switch_workspace()
    elif command == "move":
        move_container_to_selected_workspace()


def switch_workspace(name):
    proc.check_call(["i3-msg", "workspace", name])


def move_container_to_workpace(workspace):
    proc.check_call([
        "i3-msg", "move", "container", "to", "workspace", workspace,
    ])


def select_workspace():
    rofi_cmd = ["rofi", "-dmenu", "-p", "workspace: "]
    p = proc.Popen(rofi_cmd, stdin=proc.PIPE, stdout=proc.PIPE, universal_newlines=True)
    stdout, _ = p.communicate(input="\n".join(workspace_names()))
    if p.returncode == 0:
        return stdout.rstrip()
    return ""


def find_free_workspace():
    used = set(workspace_numbers())
    if not used:
        return MIN_WORKSPACE
    ws = MIN_WORKSPACE
    while True:
        if ws not in used:
            return ws
        ws += 1


def workspaces():
    output = proc.check_output(["i3-msg", "-t", "get_workspaces"])
    return json.loads(output.decode('ascii'))


def workspace_numbers():
    for workspace in workspaces():
        num = workspace["num"]
        if num >= 0:
            yield num


def workspace_names():
    for workspace in workspaces():
        yield workspace["name"]


if __name__ == '__main__':
    main()
